package convertidorgf.jhalmarpiloso.facci.practicagetrecuperacion.Actividades;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


import convertidorgf.jhalmarpiloso.facci.practicagetrecuperacion.Adaptador.AdaptadorProductos;
import convertidorgf.jhalmarpiloso.facci.practicagetrecuperacion.Modelo.Productos;
import convertidorgf.jhalmarpiloso.facci.practicagetrecuperacion.R;

public class MainActivity extends AppCompatActivity {

    private static final String URL_PRODUCTO = "http://10.22.29.171:3000/productos";
    private RecyclerView recyclerView;
    private ArrayList<Productos> productosArrayList;
    private ProgressDialog progressDialog;
    private AdaptadorProductos adaptadorProductos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView)findViewById(R.id.RecyclerProductos);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        productosArrayList = new ArrayList<>();
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("CARGANDO...");
        progressDialog.show();
        adaptadorProductos = new AdaptadorProductos(productosArrayList);

        DatosGenerales();

    }

    private void DatosGenerales() {

        StringRequest stringRequest = new StringRequest(
                Request.Method.GET, URL_PRODUCTO, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //repuesta de que todo esta bien y me trae datos
                progressDialog.dismiss();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i<jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        Productos productos = new Productos();
                        productos.setName(jsonObject.getString("name"));
                        productos.setDescripcion(jsonObject.getString("descripcion"));
                        productos.setEstado(String.valueOf(jsonObject.getBoolean("estado")));
                        productos.setFoto(jsonObject.getString("foto"));
                        productos.setMarca(jsonObject.getString("marca"));
                        productos.setPrecio(jsonObject.getString("precio"));
                        productos.setId(jsonObject.getString("id"));

                        if (productos.getEstado().equals("true")){
                            productosArrayList.add(productos);
                        }
                    }
                    recyclerView.setAdapter(adaptadorProductos);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // repuesta de que ocurrio un error mientras se hacia la petición
                Toast.makeText(MainActivity.this, "ERROR", Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
